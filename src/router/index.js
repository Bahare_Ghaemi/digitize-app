import {
  createRouter,
  createWebHistory
} from 'vue-router'

const routes = [{
    path: '/',
    name: 'HomeView',
    component: () => import('../views/HomeView.vue'),
    // children: [
    //   {
    //     path: '/',
    //     name: 'ProductsView',
    //     components: {
    //       ProductsView: () => import('../views/ProductsView.vue'),
    //       default: () => import('../views/HomeView.vue'),
    //     }
    //   }
    // ]
  },
  {
    path: '/shop/:meta',
    name: 'ProductView',
    component: () => import('../views/ProductView.vue'),
    props: true
  },
  {
    path: '/category',
    name: 'CategoryView',
    component: () => import('../views/CategoryView.vue'),
  },
  {
    path: '/category/:cat',
    name: 'FilterProByCat',
    component: () => import('../views/FilterProByCat.vue'),
    props: true,
  },
  {
    path: '/category/:brand/:cat',
    name: 'FilterProByBrand',
    component: () => import('../views/FilterProByBrand.vue'),
    props: true,
  },
  {
    path: '/cart',
    name: 'CartView',
    component: () => import('../views/CartView.vue')
  },
  {
    path: '/likes',
    name: 'LikesView',
    component: () => import('../views/LikesView.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router