import plugins from './plugins.js'
import {
  createStore
} from 'vuex'
console.log(location.pathname)
export default createStore({
  state: {
    categories: [{
        title: 'موبایل',
        title1: 'mobile',
        img: require('../assets/images/svgs/mobile-cat.svg'),
        img1: require('../assets/images/svgs/mobile-filter.svg'),
        brands: [{
            name: 'اپل',
            name1: 'Apple',
            img: require('../assets/images/svgs/apple.svg')
          },
          {
            name: 'شیائومی',
            name1: 'Xiaomi',
            img: require('../assets/images/svgs/xiaomi.svg')
          },
          {
            name: 'سامسونگ',
            name1: 'Samsong',
            img: require('../assets/images/svgs/samsong.png')
          },
          {
            name: 'جی پلاس',
            name1: 'GPlus',
            img: require('../assets/images/svgs/gplus.png')
          },
          {
            name: 'هواوی',
            name1: 'Hoawei',
            img: require('../assets/images/svgs/huawei.png')
          }
        ]
      },
      {
        title: 'لپ تاپ',
        title1: 'laptop',
        img: require('../assets/images/svgs/laptop-cat.svg'),
        img1: require('../assets/images/svgs/laptop-filter.svg'),
        brands: [{
            name: 'ایسوس',
            name1: 'Asus',
            img: require('../assets/images/svgs/asus.svg')
          },
          {
            name: 'اپل',
            name1: 'Apple',
            img: require('../assets/images/svgs/apple.svg')
          },
          {
            name: 'لنوو',
            name1: 'Lenovo',
            img: require('../assets/images/svgs/lenovo.svg')
          },
          {
            name: 'ایسر',
            name1: 'Acer',
            img: require('../assets/images/svgs/acer.png')
          },
          {
            name: 'اچ پی',
            name1: 'HP',
            img: require('../assets/images/svgs/hp.png')
          }
        ]
      },
      {
        title: 'ساعت هوشمند',
        title1: 'smart-watch',
        img: require('../assets/images/svgs/watch-cat.svg'),
        img1: require('../assets/images/svgs/watch-filter.svg'),
        brands: [{
            name: 'اپل',
            name1: 'Apple',
            img: require('../assets/images/svgs/apple.svg')
          },
          {
            name: 'شیائومی',
            name1: 'Xiaomi',
            img: require('../assets/images/svgs/xiaomi.svg')
          },
        ]
      },
    ],

    products: [{
        title: 'آیفون 11 پرومکس 256 گیگ',
        meta: 'آیفون_11_پرومکس_256_گیگ',
        english: 'Apple iphone 11 pro max 256GB',
        image: require('../assets/images/pngs/iphone11-256.png'),
        price: 28140000,
        perPrice: '۲۸,۱۴۰,۰۰۰',
        brand: 'اپل',
        colors: [{
          name: 'مشکی',
          code: '#000'
        }, {
          name: 'سفید',
          code: '#fff'
        }],
        screenTech: 'Liquid Retina',
        size: 6.1,
        resulotion: '12 مگاپیکسل',
        system: 'iOS 13',
        category: 'موبایل',
        dimensions: '۸.۳x۷۵.۷x۱۵۰.۹',
        storage: '256 گیگابایت',
        weight: 194,
        comments: [{
            author: 'محمد پارسا بخشی',
            text: 'سلام خیلی عالی راضیم'
          },
          {
            author: 'یسنا عسگری',
            text: 'عالی'
          },
          {
            author: 'کاربر دیجی تایز',
            text: 'گوشی به موقع به دستم رسید و بهترین قیمت رو دیجیتایز میداد'
          },
          {
            author: 'حمید رضا امامی جو',
            text: 'در مجموع خوب است ولی گران است'
          },
        ],
        points: [{
          key: 'کیفیت ساخت',
          value: '70'
        }, {
          key: 'ارزش خرید به نسبت قیمت',
          value: '84'
        }, {
          key: 'نوآوری',
          value: '90'
        }, {
          key: 'امکانات و قابلیت ها',
          value: '80'
        }, {
          key: 'سهولت استفاده',
          value: '70'
        }],
        description: 'شکی نیست که گوشی‌های iPhone 11 Pro و 11 Pro Max محصولات محبوب و مشهور امسال هستند و تقریباً تمام توجه کاربران و علاقه‌مندان به دنیای فناوری اطلاعات را به خود معطوف کرده‌اند. اما نمی‌توان از این حقیقت به‌سادگی گذشت که آیفون 11 ارزش خرید بالایی دارد. شاید تصور کنید با قیمت پایین‌تری که iPhone 11 نسبت به دیگر برادران خود دارد، ویژگی‌های بسیاری هم در آن نادیده گرفته شده‌اند اما در عمل اصلاً چنین نیست؛ همه طرفداران اپل به صفحه‌نمایش باکیفیت، باتری کمی ‌قوی‌تر و امکان زوم اپتیکال نیاز ندارند. پس اگر شما هم می‌خواهید «آیفون جدید» اپل را داشته باشید و نمی‌خواهید هزینه زیادی صرف کنید، با این بررسی مختصر از Apple iPhone 11 همراه باشید.'
      },
      {
        title: 'آیفون 12 - 128 گیگ',
        meta: 'آیفون_12_128_گیگ',
        english: 'Apple iphone 12 - 128GB',
        image: require('../assets/images/pngs/iphone-12-128.png'),
        price: 34989000,
        perPrice: '۳۴,۹۸۹,۰۰۰',
        brand: 'اپل',
        colors: [{
            name: 'مشکی',
            code: '#000'
          }, {
            name: 'سفید',
            code: '#fff'
          },
          {
            name: 'آبی',
            code: '#1e40af'
          },
        ],
        screenTech: 'Super Retina XDR OLED',
        size: 6.1,
        resulotion: '12 مگاپیکسل',
        system: 'iOS 14.1',
        category: 'موبایل',
        dimensions: '۷.۴x۷۱.۵x۱۴۶.۷ میلی‌متر',
        storage: '128 گیگابایت',
        weight: 164,
        comments: [{
            author: 'کاربر دیجی تایز',
            text: 'بهترین ، دیگه حرفی برا گفتن نذاشته'
          },
          {
            author: 'امیرعلی صادقپور',
            text: 'درکل راضی و محصول فوق العاده خوبی هست'
          },
        ],
        points: [{
          key: 'کیفیت ساخت',
          value: '95'
        }, {
          key: 'ارزش خرید به نسبت قیمت',
          value: '85'
        }, {
          key: 'نوآوری',
          value: '92'
        }, {
          key: 'امکانات و قابلیت ها',
          value: '80'
        }, {
          key: 'سهولت استفاده',
          value: '80'
        }],
        description: 'گوشی موبایل iPhone 12 A2404 CH پرچم‌دار جدید شرکت اپل است که با چند ویژگی جدید و دوربین دوگانه روانه بازار شده است. اپل برای ویژگی‌ها و طراحی کلی این گوشی از همان فرمول چند سال اخیرش استفاده کرده است. نمایشگر آیفون 12 به پنل Super Retina مجهز ‌شده است تا تصاویر بسیار مطلوبی را به کاربر عرضه کند. این نمایشگر رزولوشن بسیار بالایی دارد؛ به‌طوری‌که در اندازه­‌ی 6.1 اینچی‌اش، حدود 460 پیکسل را در هر اینچ جا داده است که دقیقاً با تراکم پیکسلی iPhone XS برابر است. قاب پشتی آیفون جدید هم از شیشه ساخته‌ شده تا هم گوشی مشکل آنتن‌‌دهی نداشته باشد و هم امکان شارژ بی‌‌سیم باتری در این گوشی وجود داشته باشد. البته قابی فلزی این بدنه شیشه‌ای را در خود جای داده است. این بدنه­‌ی زیبا در مقابل خط‌‌وخش مقاومت زیادی دارد؛ پس خیالتان از این بابت که آب و گردوغبار به‌‌راحتی روی آیفون 12 تأثیر نمی‌‌گذارد، راحت باشد. علاوه‌براین لکه و چربی هم روی این صفحه‌نمایش باکیفیت تأثیر چندانی ندارند اما این هم پایان کار نیست، آیفون جدید می‌تواند به مدت 30 دقیقه در عمق 6 متری آب دوام بیاورد. تشخیص چهره با استفاده از دوربین جلو دیگر ویژگی است که در آیفون جدید اپل به کار گرفته شده است.'
      },
      {
        title: 'آیفون 13 - 256 گیگ',
        meta: 'آیفون_13_256_گیگ',
        english: 'Apple iphone 13 - 256GB',
        image: require('../assets/images/pngs/iphone13-removebg-preview.png'),
        price: 43699000,
        perPrice: '۴۳,۶۹۹,۰۰۰',
        brand: 'اپل',
        colors: [{
          name: 'صورتی',
          code: '#f472b6'
        }, {
          name: 'سبز',
          code: '#15803d'
        }, ],
        screenTech: 'Super Retina XDR OLED',
        size: 6.1,
        resulotion: '12 مگاپیکسل',
        system: 'iOS 15',
        category: 'موبایل',
        dimensions: '۱۴۶.۷x۷۱.۵x۷.۶۵',
        storage: '256 گیگابایت',
        weight: 174,
        comments: [{
            author: 'پوریا فرزانه',
            text: 'من خریدم عالیه'
          },
          {
            author: 'کاربر دیجی تایز',
            text: 'گوشی نان اکتیو و به راحتی ریجستری شد.'
          },
          {
            author: 'سید محسن حسینی',
            text: 'لطفا موجود کنیددد رنگ سفید رو من میخوام بخرم'
          },
        ],
        points: [{
          key: 'کیفیت ساخت',
          value: '90'
        }, {
          key: 'ارزش خرید به نسبت قیمت',
          value: '70'
        }, {
          key: 'نوآوری',
          value: '95'
        }, {
          key: 'امکانات و قابلیت ها',
          value: '80'
        }, {
          key: 'سهولت استفاده',
          value: '100'
        }],
        description: 'گوشی موبایل «iPhone 13» پرچم‌دار جدید شرکت اپل است که با چند ویژگی جدید و دوربین دوگانه روانه بازار شده است. اپل برای ویژگی‌ها و طراحی کلی این گوشی از همان فرمول چند سال اخیرش استفاده کرده است. نمایشگر آیفون 13 به پنل Super Retina مجهز ‌شده است تا تصاویر بسیار مطلوبی را به کاربر عرضه کند. این نمایشگر رزولوشن بسیار بالایی دارد؛ به‌طوری‌که در اندازه­‌ی 6.1 اینچی‌اش، حدود 460 پیکسل را در هر اینچ جا داده است. امکان شارژ بی‌‌سیم باتری در این گوشی وجود دارد. روکش سرامیکی صفحه‌نمایش این گوشی می‌تواند انقلابی در محافظت به‌پا کند. این گوشی ضدآب و ضد گردوخاک است. بدنه­ زیبا iPhone 13 در مقابل خط‌‌وخش مقاومت زیادی دارد؛ پس خیالتان از این بابت که آب و گردوغبار به‌‌راحتی روی آیفون 13 تأثیر نمی‌‌گذارد، راحت باشد. علاوه‌براین لکه و چربی هم روی این صفحه‌نمایش باکیفیت تأثیر چندانی ندارند. تشخیص چهره با استفاده از دوربین جلو دیگر ویژگی است که در آیفون جدید اپل به کار گرفته شده است. قابلیت اتصال به شبکه­‌های 4G و 5G، بلوتوث نسخه‌ 5، نسخه­‌ 15 از iOS دیگر ویژگی‌های این گوشی هستند. ازنظر سخت‌‌افزاری هم این گوشی از تراشه­‌ی جدید A15 بهره می‌برد که دارای 15 میلیارد ترانزیستور است که دارای کنترل گرمای مطلوبی بوده که تا بتواند علاوه بر کارهای معمول، از قابلیت‌های جدید واقعیت مجازی که اپل این روزها روی آن تمرکز خاصی دارد، پشتیبانی کند.'
      },
      {
        title: 'ساعت هوشمند اپل سری SE',
        meta: 'SE_ساعت_هوشمند_اپل_سری',
        english: 'Apple Watch Series SE',
        image: require('../assets/images/pngs/smart-watch-apple.png'),
        price: 9219000,
        perPrice: '۹,۲۱۹,۰۰۰',
        brand: 'اپل',
        colors: [{
            name: 'مشکی',
            code: '#000'
          }, {
            name: 'سفید',
            code: '#fff'
          },
          {
            name: 'طلایی',
            code: '#ca8a04'
          },
        ],
        use: 'روزمره، ورزشی',
        form: 'مستطیل',
        band: 'سیلیکون',
        useFor: 'آقایان و بانوان',
        category: 'ساعت هوشمند',
        dimensions: '۱۰.۴×۳۸×۴۴',
        weight: '36.4',
        battery: '۴۲۰ میلی آمپر ساعت',
        band: 'سیلیکون',
        body: 'آلومینیوم',
        comments: [{
            author: 'سید عرفان موسوی',
            text: 'خیلی خوبه،صد در صد اپل نیازی به تعریف نداره'
          },
          {
            author: 'بهناز نوروزعلی',
            text: 'اگر گوشی اپل دارین در خرید آن شک نکنید'
          },
        ],
        points: [{
          key: 'کیفیت ساخت',
          value: '90'
        }, {
          key: 'ارزش خرید به نسبت قیمت',
          value: '95'
        }, {
          key: 'نوآوری',
          value: '95'
        }, {
          key: 'امکانات و قابلیت ها',
          value: '70'
        }, {
          key: 'سهولت استفاده',
          value: '80'
        }],
        description: 'ساعت هوشمند اپل واچ سری SE 2021 مدل 44mm Aluminum Case از ساعت‌های هوشمند زیبای این برند است.این اپل‌واچ 44 میلی‌متری ویژگی‌های خاصی دارد که می‌توان از میان آن‌ها به مقاومت در برابر آب تا عمق 50 متری اشاره کرد. مقاومت در برابر آب این مدل موجب می‌شود، کاربر هنگام بارش باران یا شست‌وشوی دست، بدون نگرانی از این گجت استفاده ‌کند، همچنین قادر به استفاده از آن برای برخی ورزش‌های آبی (نظیر شنا در آب‌های کم‌عمق و کوتاه‌مدت) باشد..اپل واچ سری SE 2021 از تراشه S5 بهره می‌برد که بنابر گفته‌های کارشناسان اپل تا 2 برابر سرعت بیشتری نسبت به اپل واچ سری 3 خواهد داشت. سیستم مسیریابی جی‌پی‌اس داخلی از قابلیت‌های جذاب این مدل از سری جدید اپل‌واچ هستند.'
      },
      {
        title: 'آیفون 13 پرومکس 1 ترابایت',
        meta: 'آیفون_13_پرومکس_1_ترابایت',
        english: 'Apple iphone 13 pro max - 1TB',
        image: require('../assets/images/pngs/iphone13-1.png'),
        price: 68700000,
        perPrice: '۶۸,۷۰۰,۰۰۰',
        brand: 'اپل',
        colors: [{
            name: 'سفید',
            code: '#fff'
          }, {
            name: 'سبز',
            code: '#4ade80'
          },
          {
            name: 'خاکستری',
            code: '#a1a1aa'
          },
          {
            name: 'آبی روشن',
            code: '#0ea5e9'
          },
        ],
        screenTech: 'Super Retina XDR OLED',
        size: 6.7,
        resulotion: '12 مگاپیکسل',
        system: 'iOS 15',
        category: 'موبایل',
        dimensions: '۱۶۰.۸x۷۸.۱x۷.۷',
        storage: '1 ترابایت',
        weight: 240,
        comments: [{
            author: 'کاربر دیجیتایز',
            text: 'نامبروانه فقط گرونه'
          },
          {
            author: 'عرفان شریفی',
            text: 'واقعا بهترین گوشی هست که تا الان توی بازار اومدن امیدوارم آیفون چهارده بهتر باشه'
          },
          {
            author: 'سید عرفان موسوی',
            text: 'خیلی خوبه،صد در صد اپل نیازی به تعریف نداره'
          },
          {
            author: 'کاربر دیجیتایز',
            text: 'اگر گوشی اپل دارین در خرید آن شک نکنید'
          },
          {
            author: 'محمد رستگار',
            text: 'من خریدم خیلی با کیفت و عالی بود وبه موقع رسید'
          },
          {
            author: 'مرضیه کریمی',
            text: 'خوبه یعنی عالیعههه ولی حس میکنم کیفیت دوربین نسبت به مدل قبلی فرق کرده'
          },
        ],
        points: [{
          key: 'کیفیت ساخت',
          value: '80'
        }, {
          key: 'ارزش خرید به نسبت قیمت',
          value: '95'
        }, {
          key: 'نوآوری',
          value: '100'
        }, {
          key: 'امکانات و قابلیت ها',
          value: '85'
        }, {
          key: 'سهولت استفاده',
          value: '100'
        }],
        description: 'بالاخره بعد از شایعات، شاهد رونمایی جدید‌ترین گوشی‌های هوشمند اپل در قالب خانواده آیفون 13 بودیم. آیفون 13 پرو مکس، آیفون 13 پرو، آیفون 13 و آیفون 13 مینی به‌عنوان جدید‌ترین گوشی‌های هوشمند این شرکت معرفی شدند. آیفون 13 پرو مکس بدون شک به مشخصات فنی قدرتمند‌تری به نسبت ما‌بقی اعضای این خانواده مجهز شده است. از نظر طراحی تفاوت چندانی با نسل قبلی پرچمداران این شرکت شاهد نبودیم. تنها در نمای رو به رویی این بار اپل از ناچ با عرض کمتری به نسبت نسل قبلی بهره برده است. آیفون 13 پرو مکس به صفحه‌نمایشی 6.7 اینچی با رزولوشن 1284×2778 پیکسل مجهز شده است. برای اولین بار در تاریخ گوشی‌های هوشمند اپل، صفحه‌نمایش در نظر گرفته شده برای آیفون 13 پرو مکس توانایی ارائه نرخ بروزرسانی 120 هرتز را دارد که بدون شک می تواند صفحه‌نمایشی روان‌تر و به مراتب با‌کیفیت‌تری با نسبت نسل های قبلی ارائه می‌کند. این صفحه‌نمایش توانایی ارائه حداکثر روشنایی 1200 نیت (شمع در متر مربع) را دارد که همین امر سبب می شود تا در شرایط نوری متنوع و حتی زیر تابش مستقیم نور خورشید هم، وضوح تصویر بسیار خوبی دارد. در قسمت پشتی سه سنسور دوربین به همراه یک سنسور LIDAR با همان طراحی آیفون 12 پرو مکس قرار گرفته‌اند.'
      },
      {
        title: 'آیفون 13 - 128 گیگ',
        meta: 'آیفون_13_128_گیگ',
        english: 'Apple iphone 13 - 128GB',
        image: require('../assets/images/pngs/iphone13-128.png'),
        price: 38290000,
        perPrice: '۳۸,۲۹۰,۰۰۰',
        brand: 'اپل',
        colors: [{
            name: 'صورتی',
            code: '#f472b6'
          }, {
            name: 'سبز',
            code: '#15803d'
          },
          {
            name: 'آبی روشن',
            code: '#0ea5e9'
          },
          {
            name: 'مشکی',
            code: '#000'
          },
          {
            name: 'قرمز',
            code: '#ef4444'
          },
        ],
        screenTech: 'Super Retina XDR OLED',
        size: 6.1,
        resulotion: '12 مگاپیکسل',
        system: 'iOS 15',
        category: 'موبایل',
        dimensions: '۱۴۶.۷x۷۱.۵x۷.۶۵',
        storage: '128 گیگابایت',
        weight: 174,
        comments: [{
          author: 'رضا الماسی',
          text: 'گوشیه خوبیه تا این لحظه مشکلی براش به وجود نیومده'
        }, ],
        points: [{
          key: 'کیفیت ساخت',
          value: '80'
        }, {
          key: 'ارزش خرید به نسبت قیمت',
          value: '95'
        }, {
          key: 'نوآوری',
          value: '100'
        }, {
          key: 'امکانات و قابلیت ها',
          value: '85'
        }, {
          key: 'سهولت استفاده',
          value: '100'
        }],
        description: 'گوشی موبایل «iPhone 13» پرچم‌دار جدید شرکت اپل است که با چند ویژگی جدید و دوربین دوگانه روانه بازار شده است. اپل برای ویژگی‌ها و طراحی کلی این گوشی از همان فرمول چند سال اخیرش استفاده کرده است. نمایشگر آیفون 13 به پنل Super Retina مجهز ‌شده است تا تصاویر بسیار مطلوبی را به کاربر عرضه کند. این نمایشگر رزولوشن بسیار بالایی دارد؛ به‌طوری‌که در اندازه­‌ی 6.1 اینچی‌اش، حدود 460 پیکسل را در هر اینچ جا داده است. امکان شارژ بی‌‌سیم باتری در این گوشی وجود دارد. روکش سرامیکی صفحه‌نمایش این گوشی می‌تواند انقلابی در محافظت به‌پا کند. این گوشی ضدآب و ضد گردوخاک است. بدنه­ زیبا iPhone 13 در مقابل خط‌‌وخش مقاومت زیادی دارد؛ پس خیالتان از این بابت که آب و گردوغبار به‌‌راحتی روی آیفون 13 تأثیر نمی‌‌گذارد، راحت باشد. علاوه‌براین لکه و چربی هم روی این صفحه‌نمایش باکیفیت تأثیر چندانی ندارند. تشخیص چهره با استفاده از دوربین جلو دیگر ویژگی است که در آیفون جدید اپل به کار گرفته شده است. قابلیت اتصال به شبکه­‌های 4G و 5G، بلوتوث نسخه‌ 5، نسخه­‌ 15 از iOS دیگر ویژگی‌های این گوشی هستند. ازنظر سخت‌‌افزاری هم این گوشی از تراشه­‌ی جدید A15 بهره می‌برد که دارای 15 میلیارد ترانزیستور است که دارای کنترل گرمای مطلوبی بوده که تا بتواند علاوه بر کارهای معمول، از قابلیت‌های جدید واقعیت مجازی که اپل این روزها روی آن تمرکز خاصی دارد، پشتیبانی کند.'
      },
      {
        title: 'ساعت هوشمند شیائومی مدل MI WATCH 2021',
        meta: 'MI WATCH 2021_ساعت_هوشمند_شیائومی_مدل',
        english: 'Xiaomi Smart Watch Series MI WATCH 2021',
        image: require('../assets/images/pngs/smart-watch-xiaomi.png'),
        price: 3135000,
        perPrice: '۳,۱۳۵,۰۰۰',
        brand: 'شیائومی',
        colors: [{
            name: 'مشکی',
            code: '#000'
          }, {
            name: 'بژ',
            code: '#ffedd5'
          },
          {
            name: 'سرمه ای',
            code: '#1e3a8a'
          },
        ],
        use: 'رسمی، روزمره، ورزشی',
        form: 'مستطیل',
        useFor: 'آقایان و بانوان',
        category: 'ساعت هوشمند',
        dimensions: '۴۵.۹×۵۳.۳۵×۱۱.۸',
        weight: '32',
        battery: '۴۵۰ میلی آمپر ساعت',
        band: 'سیلیکون',
        body: 'پلی کربنات',
        comments: [{
            author: 'کاربر دیجی تایز',
            text: 'به نسبت قیمت امکاناتش خوب هست و پشیمان نمیشین.'
          },
          {
            author: 'علیرضا علیپور',
            text: 'به نسبت قیمت امکاناتش خوب هست و پشیمان نمیشین.'
          },
          {
            author: 'علی اصغری',
            text: 'زیباست و تنها نکته ضعف این هست که زبان فارسی ندارد'
          },
        ],
        points: [{
          key: 'کیفیت ساخت',
          value: '80'
        }, {
          key: 'ارزش خرید به نسبت قیمت',
          value: '85'
        }, {
          key: 'نوآوری',
          value: '97'
        }, {
          key: 'امکانات و قابلیت ها',
          value: '80'
        }, {
          key: 'سهولت استفاده',
          value: '90'
        }],
        description: 'ساعت‌های هوشمند شیائومی از سری لوازم جانبی جذاب و پرکاربردی هستند که همواره طرفداران این کمپانی برای رونمایی از آن‌ در کنار سایر دستگاه‌های شیائومی انتظار می‌کشند. MI WATCH تولید 2021 جدید ترین ساعت هوشمند کمپانی شیائومی است که روانه بازار شده است. این ساعت دارای یک صفحه نمایش گرد بزرگ، روشن و بسیار با کیفیت است و طراحی ساده و در عین حال خاص و چشم نوازی دارد. این ساعت با یک باتری لیتیوم یون با ظرفیت 420 میلی آمپر ساعت کار می کند. الگوریتم های بهینه سازی مصرف Al، دوام باتری را افزایش می دهند و شما می توانید در این نسخه با یک بار شارژ کامل و استفاده معمولی تا 16 روز با ساعت کار کنید. علاوه بر این شارژر مغناطیسی، باتری را با سرعت بالایی شارژ می کند.. از دیگر ویژگی های قابل توجه این محصول باید به پشتیبانی از سیستم GPS، پشتیبانی از ورزش های متنوع و امکان نظارت بر ضربان قلب و سطح اکسیژن خون اشاره کرد. Mi Watch دارای یک صفحه نمایش گرد 1.39 اینچی همیشه روشن است که قابلیت نمایش اطلاعات را حتی در زیر نور مستقیم آفتاب دارد. این نمایشگر از نوع AMOLED بوده و تماس های دریافتی، نوتیفیکیشن ها، گزینه های تناسب اندام و دیگر اطلاعات مهم را با وضوح بالا به شما نمایش خواهد داد. به دلیل استفاده از قاب جدید، وزن آن فقط 32 گرم است. وزن کم ساعت به شما اجازه می دهد تا انرژی خود را آزاد کرده و فعالیت های ورزشی لذت بخشی را تجربه کنید.'
      },
    ],

    // all products in cart
    cart: JSON.parse(localStorage.getItem('cart')) || [],

    // discount
    copons: [{
      code: 'newyear',
      percent: 10
    }, {
      code: 'yalda',
      percent: 10
    }, {
      code: 'salenomobarak',
      percent: 10
    }],

    disCountedPrice: null,

    filters: [],

  },

  getters: {
    // set filtered products
    setFilteredPros(state) {
      state.filters.forEach((filter) => {
        return state.products.filter((pro) => {
          // if (filter.key === 'برند محصول') {
          //   if (pro.brand === filter.value) {
          //     return pro
          //   }
          // }
          return pro.band === 'اپل'
        })
      });
      // return state.products.filter((pro) => {
      //   state.filters.forEach((filter) => {
      //     if (filter.key === 'برند محصول') {
      //       if (pro.brand === filter.value) {
      //         return pro
      //       }
      //     }
      //   });
      // })
    },
    // filtered products
    filteredProducts(state, getters) {
      return state.filters.length ? getters.setFilteredPros : state.products;
    },
    // total price of products in cart
    totalPrice(state) {
      return state.cart.reduce((acc, curr) => {
        return acc + curr.addedProduct.count * curr.addedProduct.product.price;
      }, 0)
    },
    // count of products in cart
    cartCount(state) {
      return state.cart.reduce((acc, curr) => {
        return acc + curr.addedProduct.count;
      }, 0)
    },

    // all products with mobile category
    mobile(state) {
      return state.products.filter((pro) => {
        return pro.category === 'موبایل'
      })
    },
    // all products with laptop category
    laptop(state) {
      return state.products.filter((pro) => {
        return pro.category === 'لپ تاپ'
      })
    },
    // all products with smart watch category
    smartWatch(state) {
      return state.products.filter((pro) => {
        return pro.category === 'ساعت هوشمند'
      })
    },

    // all mobiles with Apple brand
    appleMobile(state, getters) {
      return getters.mobile.filter((mob) => {
        return mob.brand === 'اپل'
      })
    }
  },

  mutations: {
    //add new filter
    addNewFilter(state,
      newFilter
    ) {
      console.log(newFilter);
      state.filters.push(
        newFilter
      );
      console.log(state.filters);
    },

    // add to cart
    addToCart(state, {
      addedProduct
    }) {
      state.cart.push({
        addedProduct,
      })
    },
    increaseInCart(state, pro) {
      console.log(pro);
      pro.addedProduct.count++
    },
    decreaseInCart(state, pro) {
      console.log(pro);
      if (pro.addedProduct.count > 1) {
        pro.addedProduct.count--
      } else {
        this.deleteProduct(pro);
      }
    },

    //add to cart
    increaseCount(state, {
      proIsInCart
    }) {
      console.log(proIsInCart.addedProduct)
      proIsInCart.addedProduct.count++;
      console.log(proIsInCart.addedProduct)
    },

    // deduct from cart
    decreaseCount(state, {
      proIsInCart
    }) {
      console.log(proIsInCart.addedProduct)
      if (proIsInCart.addedProduct.count > 1) {
        console.log(proIsInCart.addedProduct)
        proIsInCart.addedProduct.count--;
        console.log(proIsInCart.addedProduct)
      }

    },
    // set discount
    setDiscount(state,
      copon
    ) {
      console.log(this.state.disCountedPrice);
      this.state.disCountedPrice = this.getters.totalPrice * (100 - copon.percent) / 100
      console.log(this.state.disCountedPrice);
    },

    // delete from cart
    deleteProduct(state, {
      pro
    }) {
      state.cart.splice(state.cart.indexOf(pro), 1);
    },
  },
  actions: {},
  plugins,
})