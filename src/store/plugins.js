const localStoragePlugin = store => {
    store.subscribe((mutation,{cart}) => {
        localStorage.setItem('cart',JSON.stringify(cart))
    })
}

export default [localStoragePlugin]